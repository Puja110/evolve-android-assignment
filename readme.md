# Project Title #
My project name is 'Evolve Android Assignment'. In this project, I have implemented a simple listing and detail showcase application using the given APIs.
Third party library, Retrofit, is used for parsing an API response easily. I have used the GsonConverterFactory to directly convert json data to object.
And 'Material Design' components such as recyclerview library and cardview library have used.

### Libraries ###
* v7 recyclerview library
* v7 CardView library
* Retrofit

### Dependencies ###
* implementation 'com.android.support:cardview-v7:27.1.1'
* implementation 'com.android.support:recyclerview-v7:27.1.1'
* implementation 'com.squareup.retrofit2:retrofit:2.4.0'
* implementation 'com.squareup.retrofit2:converter-gson:2.3.0'

### Clean code architecture ###
* MVC (Model View Controller )

### Hardware requirements ###
* The application runs on any decent Android device with IceCreamSandwich and up. 

### Software Requirements ###
* The Android OS running on the devices must be of Android SDK API Level 15 and up.

### Bonus points ###
* Third party library ( Retrofit )
* 'Material Design' components ( Recyclerview and Cardview )
* Readme file
* Implementation of clean code architecture ( MVC )